﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

	float ColdownEnemy = 2f;
	float MaxColdownEnemy = 10f;
	int lvl = 0;

	float progressLvl = 0;

	public GameObject character;
	public Rigidbody2D[] TypeEnemy = new Rigidbody2D[5];


	void Start ()
	{

		ResetGame();
	}

	public void ResetGame()
	{
		ColdownEnemy = 2f;
		MaxColdownEnemy = 10f;
		lvl = 0;
	}

	void Update ()
	{
		ColdownEnemy -= Time.deltaTime;
		if(ColdownEnemy < 0)
		{
			if(lvl < 4)
			{
				progressLvl += 2;
				if(progressLvl == 100)
				{
					progressLvl = 0;
					++lvl;
				}
			}

			if(MaxColdownEnemy > 0.02f) MaxColdownEnemy -= 0.02f;
			else MaxColdownEnemy = 0.02f;
			ColdownEnemy = MaxColdownEnemy;

			for(short i = 0; i < (1+lvl)*(progressLvl/10); ++i)SpawnEnemy(Random.Range(0, lvl), Random.Range(-1.4f, 1.4f));
		}
	}

	void SpawnEnemy(int typeEnemy, float odsuniecie)
	{
		Rigidbody2D NewEnemy = Instantiate(TypeEnemy[typeEnemy], new Vector2(this.gameObject.transform.position.x + odsuniecie, this.gameObject.transform.position.y), this.gameObject.transform.rotation) as Rigidbody2D;
		NewEnemy.GetComponent<Enemy>().player = character;
	}
}
