﻿using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour {

	public GameObject player;

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Enemy")
		{
			player.SendMessage("TakeLives");
		}
	}
}
