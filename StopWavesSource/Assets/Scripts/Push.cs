﻿using UnityEngine;
using System.Collections;

public class Push : MonoBehaviour {

	Rigidbody2D cialo;

	void Start ()
	{
		cialo = this.GetComponent<Rigidbody2D>();
	}

	void Update ()
	{
		if(Input.GetKey(KeyCode.A)) cialo.velocity = new Vector2(-1f, cialo.velocity.y);
		else if(Input.GetKey(KeyCode.D)) cialo.velocity = new Vector2(1f, cialo.velocity.y);
		else cialo.velocity = new Vector2(0f, cialo.velocity.y);
	}
}
