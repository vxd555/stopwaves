﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	GUIStyle[] LabelStyle = new GUIStyle[3];
	public GameObject Game;

	bool poczatek = true;
	public int BestSecore = 0;
	public int newSecore = 0;

	void Start ()
	{
		Screen.SetResolution(1024, 600, false);
		BestSecore = PlayerPrefs.GetInt("Secore", 0);
	}

	void OnDestroy()
	{
		PlayerPrefs.SetInt("Secore", BestSecore);
	}
	void Update ()
	{
		if(Input.GetKey(KeyCode.Return))
		{
			Game.SetActive(true);
			poczatek = false;
			this.gameObject.SetActive(false);
		}
		if(Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	void OnGUI()
	{
		LabelStyle[0] = new GUIStyle(GUI.skin.label);
		LabelStyle[0].fontSize = 35;
		LabelStyle[0].alignment = TextAnchor.MiddleCenter;
		LabelStyle[1] = new GUIStyle(GUI.skin.label);
		LabelStyle[1].fontSize = 15;
		LabelStyle[1].alignment = TextAnchor.MiddleCenter;
		LabelStyle[2] = new GUIStyle(GUI.skin.label);
		LabelStyle[2].fontSize = 45;
		LabelStyle[2].alignment = TextAnchor.MiddleCenter;

		GUI.Label(new Rect(Screen.width/2-150, 50, 300, 100), "STOP WAVES", LabelStyle[2]);

		GUI.Label(new Rect(Screen.width/2-190, Screen.height/2-50, 380, 100), "Press ENTER to start", LabelStyle[0]);

		if(poczatek)
		{
			GUI.Label(new Rect(Screen.width/2-190, Screen.height/2+30, 380, 30), "CONTROL:", LabelStyle[1]);
			GUI.Label(new Rect(Screen.width/2-190, Screen.height/2+60, 380, 30), "MOVE: A, D, LeftArrow, RightArrow", LabelStyle[1]);
			GUI.Label(new Rect(Screen.width/2-190, Screen.height/2+90, 380, 30), "SHOOT: W, UpArrow, Space", LabelStyle[1]);
			GUI.Label(new Rect(Screen.width/2-190, Screen.height/2+120, 380, 30), "UPGRADE: 1 Firing Rate, 2 Movment, 3 Lives", LabelStyle[1]);
		}

		if(!poczatek) 
		{
			GUI.Label(new Rect(Screen.width/2-150, Screen.height/2+50, 300, 100), "Your Score: "+newSecore, LabelStyle[0]);
			GUI.Label(new Rect(Screen.width/2-150, Screen.height/2+80, 300, 100), "Best Score: "+BestSecore, LabelStyle[1]);
		}
	}

	public void NewSecore(int wynik)
	{
		newSecore = wynik;
		if(newSecore > BestSecore) BestSecore = newSecore;
	}
}
