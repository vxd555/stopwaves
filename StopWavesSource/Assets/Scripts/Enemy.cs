﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public AudioClip DeadSound;

	public int lives = 1;
	public int exp = 0;
	public GameObject player;

	void Start ()
	{
		
	}

	void Update ()
	{
		if(lives <= 0)
		{
			audio.clip = DeadSound;
			audio.Play();
			player.SendMessage("SecoreAdd", exp);
			Destroy(this.gameObject);
		}
	}

	public void TakeLives()
	{
		--lives;
	}

	public void SetPlayer(GameObject PlayerGame)
	{
		player = PlayerGame;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Laser")
		{
			col.gameObject.SendMessage("jep");
			TakeLives();

		}
		if(col.tag == "Player")
		{
			col.gameObject.SendMessage("TakeLives");
			Destroy(this.gameObject);
		}
	}
}
