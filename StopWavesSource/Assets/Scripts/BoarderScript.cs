﻿using UnityEngine;
using System.Collections;

public class BoarderScript : MonoBehaviour {

	public GameObject player;
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Laser")
		{
			Destroy(col);
		}
	}
}
